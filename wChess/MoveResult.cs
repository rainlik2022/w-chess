﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wChess
{
    /// <summary>
    /// 走棋的几种结果
    /// </summary>
    public enum MOVE_RESULT
    {
        NULL,//默认状态
        ENEMY_TURN,//应该敌方走棋
        CHECK_ENEMY,//对敌方将军
        CHECK_SELF,//己方被将军
        CAN_NOT_MOVE,//不能移动
        MING_JIANG//明将
    }
}
