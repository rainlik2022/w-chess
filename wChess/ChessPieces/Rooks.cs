﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace wChess.ChessPieces
{
    /// <summary>
    /// 車
    /// </summary>
    public class Rooks:ChessPiece
    {
        public Rooks(Point metaPos, TEAM team = TEAM.RED, SIDE side = SIDE.UP)
            : base("车", metaPos, team,side)
        {
            if (Team == TEAM.BLACK)
                Name = "車";
        }
        public override List<Point> NextSteps
        {
            get
            {
                List<Point> r = new List<Point>();
                r.AddRange(GetAboveSteps());
                r.AddRange(GetBelowSteps());
                r.AddRange(GetLeftSteps());
                r.AddRange(GetRightSteps());
                return r;
            }
        }

        //下方的可以移动的位置预览
        private List<Point> GetBelowSteps()
        {
            List<Point> r = new List<Point>();
            var yBelow = FixedMetaPosition.Y;
            do
            {
                if (yBelow + 1 < ChessBoard.BOARD_META_HEIGHT)
                {
                    if (ChessBoard.Matrix[yBelow + 1, FixedMetaPosition.X] == 1)
                    {
                        var p = ChessBoard.GetPieceFromMetaPos(FixedMetaPosition.X, yBelow + 1);

                        //目标位置只要没有己方棋子
                        if (p.Count > 0 && p[0].Team != Team)
                        {
                            yBelow++;
                        }
                        break;
                    }
                    yBelow++;
                }
                else
                {
                    break;
                }
            } while (true);

            for (int i = yBelow; i > FixedMetaPosition.Y; i--)
            {
                r.Add(new Point(FixedMetaPosition.X, i));
            }
            return r;
        }

        //上方的可以移动的位置预览
        private List<Point> GetAboveSteps()
        {
            List<Point> r = new List<Point>();
            var yAbove = FixedMetaPosition.Y;
            do
            {
                if (yAbove - 1 >= 0)
                {
                    if (ChessBoard.Matrix[yAbove - 1, FixedMetaPosition.X] == 1)
                    {
                        var p = ChessBoard.GetPieceFromMetaPos(FixedMetaPosition.X, yAbove - 1);
                        //目标位置只要没有己方棋子
                        if (p.Count > 0 && p[0].Team != Team)
                        {
                            yAbove--;
                        }
                        break;
                    }
                    yAbove--;
                }
                else
                {
                    break;
                }
            } while (true);

            for (int i = yAbove; i < FixedMetaPosition.Y; i++)
            {
                r.Add(new Point(FixedMetaPosition.X, i));
            }
            return r;
        }

        //右侧的可以移动的位置预览
        private List<Point> GetRightSteps()
        {
            List<Point> r = new List<Point>();
            var xRight = FixedMetaPosition.X;
            do
            {
                try
                {
                    if (xRight + 1 < ChessBoard.BOARD_META_WIDTH)
                    {
                        if (ChessBoard.Matrix[FixedMetaPosition.Y, xRight + 1] == 1)
                        {
                            var p = ChessBoard.GetPieceFromMetaPos(xRight + 1, FixedMetaPosition.Y);
                            //目标位置只要没有己方棋子
                            if (p.Count > 0 && p[0].Team != Team)
                            {
                                xRight++;
                            }
                            break;
                        }
                        xRight++;
                    }
                    else
                    {
                        break;
                    }
                }
                catch
                { }
            } while (true);

            for (int i = xRight; i > FixedMetaPosition.X; i--)
            {
                r.Add(new Point(i, FixedMetaPosition.Y));
            }
            return r;
        }
        //左侧的可以移动的位置预览
        private List<Point> GetLeftSteps()
        {
            List<Point> r = new List<Point>();
            var xLeft = FixedMetaPosition.X;
            do
            {
                if (xLeft - 1 >= 0)
                {
                    if (ChessBoard.Matrix[FixedMetaPosition.Y, xLeft - 1] == 1)
                    {
                        var p = ChessBoard.GetPieceFromMetaPos(xLeft - 1, FixedMetaPosition.Y);
                        //目标位置只要没有己方棋子
                        if (p.Count > 0 && p[0].Team != Team)
                        {
                            xLeft--;
                        }
                        break;
                    }
                    xLeft--;
                }
                else
                {
                    break;
                }
            } while (true);

            for (int i = xLeft; i < FixedMetaPosition.X; i++)
            {
                r.Add(new Point(i, FixedMetaPosition.Y));
            }
            return r;
        }
    }
}
