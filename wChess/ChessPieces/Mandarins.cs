﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace wChess.ChessPieces
{
    /// <summary>
    /// 士
    /// </summary>
    public class Mandarins:ChessPiece
    {
        public Mandarins(Point metaPos, TEAM team = TEAM.RED, SIDE side = SIDE.UP)
            : base("士", metaPos, team,side)
        {
            if (Team == TEAM.BLACK)
                Name = "仕";
        }
        public override List<Point> NextSteps
        {
            get
            {
                List<Point> r = new List<Point>();
                //左上角可移动位置预览
                if (FixedMetaPosition.X - 1 >= 3)
                {
                    if ((Side == SIDE.UP && FixedMetaPosition.Y - 1 >= 0) || (Side == SIDE.DOWN && FixedMetaPosition.Y - 1 >= 7))
                    {
                        var p = ChessBoard.GetPieceFromMetaPos(FixedMetaPosition.X - 1, FixedMetaPosition.Y - 1);
                        //目标位置若没有棋子，或者有对方棋子
                        if (p.Count == 0 || p[0].Team != Team)
                        {
                            r.Add(new Point(FixedMetaPosition.X - 1, FixedMetaPosition.Y - 1));
                        }
                    }
                }

                //右上角可移动位置预览
                if (FixedMetaPosition.X + 1 <= 5)
                {
                    if ((Side == SIDE.UP && FixedMetaPosition.Y - 1 >= 0) || (Side == SIDE.DOWN && FixedMetaPosition.Y - 1 >= 7))
                    {
                        var p = ChessBoard.GetPieceFromMetaPos(FixedMetaPosition.X + 1, FixedMetaPosition.Y - 1);
                        //目标位置若没有棋子，或者有对方棋子
                        if (p.Count == 0 || p[0].Team != Team)
                        {
                            r.Add(new Point(FixedMetaPosition.X + 1, FixedMetaPosition.Y - 1));
                        }
                    }
                }

                //右下角可移动位置预览
                if (FixedMetaPosition.X + 1 <= 5)
                {
                    if ((Side == SIDE.UP && FixedMetaPosition.Y + 1 <= 2) || (Side == SIDE.DOWN && FixedMetaPosition.Y + 1 < ChessBoard.BOARD_META_HEIGHT))
                    {
                        var p = ChessBoard.GetPieceFromMetaPos(FixedMetaPosition.X + 1, FixedMetaPosition.Y + 1);
                        //目标位置若没有棋子，或者有对方棋子
                        if (p.Count == 0 || p[0].Team != Team)
                        {
                            r.Add(new Point(FixedMetaPosition.X + 1, FixedMetaPosition.Y + 1));
                        }
                    }
                }

                //左下角可移动位置预览
                if (FixedMetaPosition.X - 1 >= 3)
                {
                    if ((Side == SIDE.UP && FixedMetaPosition.Y + 1 <= 2) || (Side == SIDE.DOWN && FixedMetaPosition.Y + 1 < ChessBoard.BOARD_META_HEIGHT))
                    {
                        var p = ChessBoard.GetPieceFromMetaPos(FixedMetaPosition.X - 1, FixedMetaPosition.Y + 1);
                        //目标位置若没有棋子，或者有对方棋子
                        if (p.Count == 0 || p[0].Team != Team)
                        {
                            r.Add(new Point(FixedMetaPosition.X - 1, FixedMetaPosition.Y + 1));
                        }
                    }
                }
                return r;
            }
        }
    }
}
