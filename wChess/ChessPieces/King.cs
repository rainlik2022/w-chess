﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace wChess.ChessPieces
{
    /// <summary>
    /// 帅
    /// </summary>
    public class King:ChessPiece
    {
        public King(Point metaPos, TEAM team = TEAM.RED, SIDE side = SIDE.UP)
            : base("将", metaPos, team,side)
        {
            if (Team == TEAM.BLACK)
                Name = "帅";
        }
        public override List<Point> NextSteps
        {
            get
            {
                //可能移动的位置集合
                List<Point> r = new List<Point>();
                if (FixedMetaPosition.X - 1 >= 3)
                    r.Add(new Point(FixedMetaPosition.X - 1, FixedMetaPosition.Y));
                if (FixedMetaPosition.X + 1 <= 5)
                    r.Add(new Point(FixedMetaPosition.X + 1, FixedMetaPosition.Y));
                if ((Side == SIDE.UP && FixedMetaPosition.Y - 1 >= 0) || (Side == SIDE.DOWN && FixedMetaPosition.Y - 1 >= 7))
                    r.Add(new Point(FixedMetaPosition.X, FixedMetaPosition.Y - 1));
                if ((Side == SIDE.UP && FixedMetaPosition.Y + 1 <= 2) || (Side == SIDE.DOWN && FixedMetaPosition.Y + 1 < ChessBoard.BOARD_META_HEIGHT))
                    r.Add(new Point(FixedMetaPosition.X, FixedMetaPosition.Y + 1));

                //考虑目标位置有没有其他棋子
                List<Point> finalResult = new List<Point>();
                foreach (var p in r)
                {
                    var ps = ChessBoard.GetPieceFromMetaPos(p.X, p.Y);
                    if (ps.Count == 1)
                    {
                        if (ps[0].Team != Team)
                        {
                            finalResult.Add(p);
                        }
                    }
                    else if (ps.Count == 0)
                    {
                        finalResult.Add(p);
                    }
                }
                return finalResult;
            }
        }
    }
}
