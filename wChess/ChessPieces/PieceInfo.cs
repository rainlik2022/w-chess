﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace wChess.ChessPieces
{
    public class PieceInfo
    {
        public Point Pos { set; get; }
        public bool EatenTag { set; get; }
    }
}
