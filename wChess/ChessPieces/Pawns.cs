﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace wChess.ChessPieces
{
    /// <summary>
    /// 兵
    /// </summary>
    public class Pawns:ChessPiece
    {
        public Pawns(Point metaPos, TEAM team = TEAM.RED, SIDE side = SIDE.UP)
            : base("卒", metaPos, team, side)
        {
            if (Team == TEAM.BLACK)
                Name = "兵";
        }
        public override List<Point> NextSteps
        {
            get
            {
                List<Point> r = new List<Point>();
                //只有在兵卒过河以后，才可以左右移动
                if ((Side == SIDE.UP && FixedMetaPosition.Y >= 5) || (Side == SIDE.DOWN && FixedMetaPosition.Y < 5))
                {
                    //左侧可移动位置预览
                    if (this.FixedMetaPosition.X - 1 >= 0)
                    {
                        var p = ChessBoard.GetPieceFromMetaPos(FixedMetaPosition.X - 1, FixedMetaPosition.Y);
                        //目标位置若没有棋子，或者有对方棋子
                        if (p.Count == 0 || p[0].Team != Team)
                        {
                            r.Add(new Point(FixedMetaPosition.X - 1, FixedMetaPosition.Y));
                        }
                    }

                    //右侧可移动位置预览
                    if (this.FixedMetaPosition.X + 1 < ChessBoard.BOARD_META_WIDTH)
                    {
                        var p = ChessBoard.GetPieceFromMetaPos(FixedMetaPosition.X + 1, FixedMetaPosition.Y);
                        //目标位置若没有棋子，或者有对方棋子
                        if (p.Count == 0 || p[0].Team != Team)
                        {
                            r.Add(new Point(FixedMetaPosition.X + 1, FixedMetaPosition.Y));
                        }
                    }
                }

                //前进方向可移动位置预览
                switch (Side)
                {
                    case SIDE.UP:
                        if (this.FixedMetaPosition.Y + 1 < ChessBoard.BOARD_META_HEIGHT)
                        {
                            var p = ChessBoard.GetPieceFromMetaPos(FixedMetaPosition.X, FixedMetaPosition.Y + 1); ;
                            //目标位置若没有棋子，或者有对方棋子
                            if (p.Count == 0 || p[0].Team != Team)
                            {
                                r.Add(new Point(FixedMetaPosition.X, FixedMetaPosition.Y + 1));
                            }
                        }
                        break;
                    case SIDE.DOWN:
                        if (this.FixedMetaPosition.Y - 1 >= 0)
                        {
                            var p = ChessBoard.GetPieceFromMetaPos(FixedMetaPosition.X, FixedMetaPosition.Y - 1);
                            //目标位置若没有棋子，或者有对方棋子
                            if (p.Count == 0 || p[0].Team != Team)
                            {
                                r.Add(new Point(FixedMetaPosition.X, FixedMetaPosition.Y - 1));
                            }
                        }
                        break;
                }
                return r;
            }
        }
    }
}
