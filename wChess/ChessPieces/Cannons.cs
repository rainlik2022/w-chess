﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace wChess.ChessPieces
{
    /// <summary>
    /// 炮
    /// </summary>
    public class Cannons:ChessPiece
    {
        public Cannons(Point metaPos, TEAM team = TEAM.RED, SIDE side = SIDE.UP)
            : base("炮", metaPos, team,side)
        {
            if (Team == TEAM.BLACK)
                Name = "砲";
        }
        public override List<Point> NextSteps
        {
            get
            {
                List<Point> r = new List<Point>();
                r.AddRange(GetAboveSteps());
                r.AddRange(GetBelowSteps());
                r.AddRange(GetLeftSteps());
                r.AddRange(GetRightSteps());
                return r;
            }
        }

        //上方可以移动的位置预览
        private List<Point> GetAboveSteps()
        {
            List<Point> r = new List<Point>();

            //当前棋子（“炮”）上方所有位置上存在棋子的位置集合
            List<Point> piecesAbove = new List<Point>();
            for (int i = FixedMetaPosition.Y; i > 0; i--)
            {
                if (ChessBoard.Matrix[i - 1, FixedMetaPosition.X] == 1)
                    piecesAbove.Add(new Point(FixedMetaPosition.X, i - 1));
            }

            switch (piecesAbove.Count)
            {
                case 0:
                    for (int i = 0; i < FixedMetaPosition.Y; i++)
                    {
                        r.Add(new Point(FixedMetaPosition.X, i));
                    }
                    break;
                case 1:
                    for (int i = piecesAbove[0].Y + 1; i < FixedMetaPosition.Y; i++)
                    {
                        r.Add(new Point(FixedMetaPosition.X, i));
                    }
                    break;
                default:
                    for (int i = piecesAbove[0].Y + 1; i < FixedMetaPosition.Y; i++)
                    {
                        r.Add(new Point(FixedMetaPosition.X, i));
                    }
                    var p = ChessBoard.GetPieceFromMetaPos(piecesAbove[1].X, piecesAbove[1].Y);
                    if (p.Count > 0 && p[0].Team != Team)
                    {
                        r.Add(piecesAbove[1]);
                    }
                    break;
            }
            return r;
        }

        //下方可以移动的位置预览
        private List<Point> GetBelowSteps()
        {
            List<Point> r = new List<Point>();

            //当前棋子（“炮”）下方所有位置上存在棋子的位置集合
            List<Point> piecesBelow = new List<Point>();
            for (int i = FixedMetaPosition.Y; i + 1 < ChessBoard.BOARD_META_HEIGHT; i++)
            {
                if (ChessBoard.Matrix[i + 1, FixedMetaPosition.X] == 1)
                    piecesBelow.Add(new Point(FixedMetaPosition.X, i + 1));
            }

            switch (piecesBelow.Count)
            {
                case 0:
                    for (int i = FixedMetaPosition.Y + 1; i < ChessBoard.BOARD_META_HEIGHT; i++)
                    {
                        r.Add(new Point(FixedMetaPosition.X, i));
                    }
                    break;
                case 1:
                    for (int i = FixedMetaPosition.Y + 1; i < piecesBelow[0].Y; i++)
                    {
                        r.Add(new Point(FixedMetaPosition.X, i));
                    }
                    break;
                default:
                    for (int i = FixedMetaPosition.Y + 1; i < piecesBelow[0].Y; i++)
                    {
                        r.Add(new Point(FixedMetaPosition.X, i));
                    }
                    var p = ChessBoard.GetPieceFromMetaPos(piecesBelow[1].X, piecesBelow[1].Y);
                    if (p.Count > 0 && p[0].Team != Team)
                    {
                        r.Add(piecesBelow[1]);
                    }
                    break;
            }
            return r;
        }

        //左侧可以移动的位置预览
        private List<Point> GetLeftSteps()
        {
            List<Point> r = new List<Point>();

            //当前棋子（“炮”）左侧所有位置上存在棋子的位置集合
            List<Point> piecesLeft = new List<Point>();
            for (int i = FixedMetaPosition.X; i - 1 >= 0; i--)
            {
                if (ChessBoard.Matrix[FixedMetaPosition.Y, i - 1] == 1)
                    piecesLeft.Add(new Point(i - 1, FixedMetaPosition.Y));
            }

            switch (piecesLeft.Count)
            {
                case 0:
                    for (int i = 0; i < FixedMetaPosition.X; i++)
                    {
                        r.Add(new Point(i, FixedMetaPosition.Y));
                    }
                    break;
                case 1:
                    for (int i = FixedMetaPosition.X - 1; i > piecesLeft[0].X; i--)
                    {
                        r.Add(new Point(i, FixedMetaPosition.Y));
                    }
                    break;
                default:
                    for (int i = FixedMetaPosition.X - 1; i > piecesLeft[0].X; i--)
                    {
                        r.Add(new Point(i, FixedMetaPosition.Y));
                    }
                    var p = ChessBoard.GetPieceFromMetaPos(piecesLeft[1].X, piecesLeft[1].Y);
                    if (p.Count > 0 && p[0].Team != Team)
                    {
                        r.Add(piecesLeft[1]);
                    }
                    break;
            }
            return r;
        }

        //右侧可以移动的位置预览
        private List<Point> GetRightSteps()
        {
            List<Point> r = new List<Point>();

            //当前棋子（“炮”）左侧所有位置上存在棋子的位置集合
            List<Point> piecesRight = new List<Point>();
            for (int i = FixedMetaPosition.X; i + 1 < ChessBoard.BOARD_META_WIDTH; i++)
            {
                if (ChessBoard.Matrix[FixedMetaPosition.Y, i + 1] == 1)
                    piecesRight.Add(new Point(i + 1, FixedMetaPosition.Y));
            }

            switch (piecesRight.Count)
            {
                case 0:
                    for (int i = FixedMetaPosition.X + 1; i < ChessBoard.BOARD_META_WIDTH; i++)
                    {
                        r.Add(new Point(i, FixedMetaPosition.Y));
                    }
                    break;
                case 1:
                    for (int i = FixedMetaPosition.X + 1; i < piecesRight[0].X; i++)
                    {
                        r.Add(new Point(i, FixedMetaPosition.Y));
                    }
                    break;
                default:
                    for (int i = FixedMetaPosition.X + 1; i < piecesRight[0].X; i++)
                    {
                        r.Add(new Point(i, FixedMetaPosition.Y));
                    }
                    var p = ChessBoard.GetPieceFromMetaPos(piecesRight[1].X, piecesRight[1].Y);

                    if (p.Count > 0 && p[0].Team != Team)
                    {
                        r.Add(piecesRight[1]);
                    }
                    break;
            }
            return r;
        }
    }
}
