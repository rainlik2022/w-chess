﻿namespace wChess
{
    partial class MainPad
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPad));
            this.chessboard = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.infoTip = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.stepTip = new System.Windows.Forms.ListBox();
            this.nextTips = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.chessboard)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chessboard
            // 
            this.chessboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.chessboard.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chessboard.BackgroundImage")));
            this.chessboard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chessboard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chessboard.Location = new System.Drawing.Point(4, 4);
            this.chessboard.Margin = new System.Windows.Forms.Padding(4);
            this.chessboard.Name = "chessboard";
            this.tableLayoutPanel1.SetRowSpan(this.chessboard, 2);
            this.chessboard.Size = new System.Drawing.Size(679, 675);
            this.chessboard.TabIndex = 0;
            this.chessboard.TabStop = false;
            this.chessboard.SizeChanged += new System.EventHandler(this.chessboard_SizeChanged);
            this.chessboard.Paint += new System.Windows.Forms.PaintEventHandler(this.chessboard_Paint);
            this.chessboard.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chessboard_MouseDown);
            this.chessboard.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chessboard_MouseMove);
            this.chessboard.MouseUp += new System.Windows.Forms.MouseEventHandler(this.chessboard_MouseUp);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.infoTip});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1002, 29);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // infoTip
            // 
            this.infoTip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.infoTip.Name = "infoTip";
            this.infoTip.Size = new System.Drawing.Size(181, 24);
            this.infoTip.Text = "Welcome to wChess";
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.toolStripContainer1.ContentPanel.Controls.Add(this.tableLayoutPanel1);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitter1);
            this.toolStripContainer1.ContentPanel.Margin = new System.Windows.Forms.Padding(4);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1002, 683);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Margin = new System.Windows.Forms.Padding(4);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1002, 712);
            this.toolStripContainer1.TabIndex = 2;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // stepTip
            // 
            this.stepTip.BackColor = System.Drawing.Color.Black;
            this.stepTip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stepTip.ForeColor = System.Drawing.Color.Lime;
            this.stepTip.FormattingEnabled = true;
            this.stepTip.ItemHeight = 18;
            this.stepTip.Location = new System.Drawing.Point(691, 33);
            this.stepTip.Margin = new System.Windows.Forms.Padding(4);
            this.stepTip.Name = "stepTip";
            this.stepTip.Size = new System.Drawing.Size(303, 646);
            this.stepTip.TabIndex = 3;
            // 
            // nextTips
            // 
            this.nextTips.AutoSize = true;
            this.nextTips.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nextTips.Font = new System.Drawing.Font("宋体", 12F);
            this.nextTips.Location = new System.Drawing.Point(691, 0);
            this.nextTips.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.nextTips.Name = "nextTips";
            this.nextTips.Size = new System.Drawing.Size(303, 29);
            this.nextTips.TabIndex = 2;
            this.nextTips.Text = "走棋提示";
            this.nextTips.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Margin = new System.Windows.Forms.Padding(4);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(4, 683);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.92523F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.07477F));
            this.tableLayoutPanel1.Controls.Add(this.chessboard, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.stepTip, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.nextTips, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.290429F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 95.70957F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(998, 683);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // MainPad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 712);
            this.Controls.Add(this.toolStripContainer1);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainPad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "wChess";
            this.Load += new System.EventHandler(this.MainPad_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainPad_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.chessboard)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox chessboard;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel infoTip;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.ListBox stepTip;
        private System.Windows.Forms.Label nextTips;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}

