﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wChess
{
    public static class StackExt
    {
        /// <summary>
        /// 按照指定策略更新堆栈的每一个元素
        /// </summary>
        /// <typeparam name="T">堆栈中元素的类型</typeparam>
        /// <param name="stack"></param>
        /// <param name="act">更新策略</param>
        public static void UpdateStack<T>(this Stack<T> stack, Func<T, T> act)
        {
            var tempArray = stack.ToArray();
            stack.Clear();
            for (int i = 0; i < tempArray.Length; i++)
            {
                tempArray[i] = act(tempArray[i]);
            }
            for (int i = tempArray.Length - 1; i >= 0; i--)
            {
                stack.Push(tempArray[i]);
            }
        }
    }
}
