﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using wChess.ChessPieces;

namespace wChess
{
    public partial class MainPad : Form
    {
        private const string welcomeTip = "欢迎光临wChess";

        private ChessBoard board = null;
        private bool isMouseDown = false;
        private ChessPiece selectedPiece = null;

        public MainPad()
        {
            InitializeComponent();
        }

        private void chessboard_Paint(object sender, PaintEventArgs e)
        {
            board.Draw(e.Graphics);
        }

        private void chessboard_SizeChanged(object sender, EventArgs e)
        {
            if (board == null)
                return;
            board.BoradWidth = chessboard.Width;
            board.BoardHeight = chessboard.Height;
            chessboard.Invalidate();//强迫重绘
        }
        private string WhoGo
        {
            get
            {
                return ChessBoard.PlayToken ? "红方走" : "黑方走";
            }
        }

        /// <summary>
        /// 显示提示信息
        /// </summary>
        /// <param name="info"></param>
        private void SetInfoTip(string info=welcomeTip)
        {
            infoTip.Text = string.Format("{0}【{1}】", info, WhoGo);
        }
        private void MainPad_Load(object sender, EventArgs e)
        {
            board = new ChessBoard(chessboard.Width, chessboard.Height,TEAM.RED);
            SetInfoTip();
        }
        
        private void chessboard_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                var metaPos = ChessBoard.GetMetaPosFromPixelPos(e.X, e.Y);
                if (metaPos == new Point(-1, -1))
                    return;
                if (selectedPiece == null)
                    return;

                //如果新位置和原位置相同，则不移动
                if (selectedPiece.FloatMetaPosition == metaPos)
                    return;
                selectedPiece.FloatMetaPosition = metaPos;
                selectedPiece.ShowPreview = false;
                chessboard.Invalidate();
            }
        }
        
        private void chessboard_MouseDown(object sender, MouseEventArgs e)
        {
            isMouseDown = true;
            var pieces = ChessBoard.GetPieceFromPixelPos(e.Location);
            if (pieces.Count > 0)
            {
                selectedPiece = pieces[0];
                selectedPiece.FixedMetaPosition = selectedPiece.FloatMetaPosition;
                selectedPiece.ShowPreview = true;
                //设置选中状态
                selectedPiece.IsActive = true;
                chessboard.Invalidate();
            }
        }
        
        private void chessboard_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
            if (selectedPiece != null)
            {
                //取消可以移动的位置预览
                selectedPiece.ShowPreview = false;

                //取消选中状态
                selectedPiece.IsActive = false;

                //移动到指定位置
                MOVE_RESULT t = selectedPiece.MoveTo(e.Location);

                //强制刷新
                chessboard.Invalidate();

                string info = null;
                switch (t)
                {
                    case MOVE_RESULT.NULL:
                        info = welcomeTip;
                        break;
                    case MOVE_RESULT.ENEMY_TURN:
                        info = "急什么？不该你走呢！";
                        break;
                    case MOVE_RESULT.CHECK_ENEMY:
                        info = "将军!";
                        break;
                    case MOVE_RESULT.CHECK_SELF:
                        info = "想什么呢？你被将军啦!";
                        break;
                    case MOVE_RESULT.MING_JIANG:
                        info = "大哥，这样你就明将啦!";
                        break;
                    case MOVE_RESULT.CAN_NOT_MOVE:
                        info = "别瞎走，这个子是那么走的么？";
                        break;
                }
                SetInfoTip(info);
                ShowAvailableSteps();
            }
            selectedPiece = null;
        }
        private void MainPad_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control)
            {
                switch(e.KeyCode)
                {
                    case Keys.Z://悔棋
                        ChessBoard.RollBack();
                        SetInfoTip();
                        break;
                    case Keys.R://重新开始棋局
                        board.ResetGame();
                        SetInfoTip();
                        break;
                    case Keys.X://交换方向
                        board.ExchangeSide();
                        break;
                }
            }
            ShowAvailableSteps();
            chessboard.Invalidate();
        }

        /// <summary>
        /// 显示可以移动的着法集合
        /// </summary>
        private void ShowAvailableSteps()
        {
            var availableSteps = ChessBoard.CheckAvailableSteps();
            stepTip.Items.Clear();
            foreach (var m in availableSteps)
            {
                var step = string.Format("【{0}】{1}({2},{3})==>({4},{5})", m.Item1.Team, m.Item1.Name, m.Item1.FixedMetaPosition.X, m.Item1.FixedMetaPosition.Y, m.Item2.X, m.Item2.Y);
                stepTip.Items.Add(step);
            }
            nextTips.Text = string.Format("走棋提示【{0}种走法】", availableSteps.Count.ToString());
            if (availableSteps.Count == 0)
            {
                var r = ChessBoard.PlayToken == true ? "红方输" : "黑方输";
                MessageBox.Show(r);
                board.ResetGame();
                chessboard.Invalidate();
            }
        }
    }
}
